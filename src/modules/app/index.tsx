import React from 'react';
import { MainRouter } from '../navigation';
import '../../sass/all.scss';

const AppContainer: React.FC = () => <MainRouter />;

export default AppContainer;
