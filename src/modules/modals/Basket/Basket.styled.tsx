import styled from 'styled-components';

export const Wrapper = styled.div``;
export const HeaderContainer = styled.div``;
export const Header = styled.h3``;
export const SubHeader = styled.h4``;
export const ItemsWrapper = styled.div``;
export const ItemsContainer = styled.div``;
export const TotalPrice = styled.p``;
export const ContinueShopping = styled.a``;
export const ButtonContainer = styled.div``;
