export const COLORS = {
  creamy: '#fffbf5',
  accent: '#4b6049',
  text: '#484847',
  white: '#ffffff'
};
