export const SIZES = {
  l: '1.5rem',
  m: '1rem',
  s: '0.85rem'
};

export const FAMILIES = {
  normal: 'Candara',
  secondary: 'Poiret One'
};

export const WEIGHTS = {
  light: '200',
  bold: '800',
  normal: '500'
};
